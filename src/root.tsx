import React, { useLayoutEffect } from 'react';
import { connect } from 'react-redux';
import { Routes, Route, useSearchParams } from 'react-router-dom';

import { HOME_GET_BASE } from './actions/base';

import Home from './views/Home';
import Load from './views/Load';
// import Demo from './views/Demo';
import Login from './views/Login';
import Register from './views/Login/Register';

const store = ({ home, base }: any) => ({ home, base });
const event = (dispatch: any) => {
  return {
    // 请求基础数据
    getBaseData(lang: any, then: any = () => {}) {
      dispatch({
        type: HOME_GET_BASE.name,
        body: { lang },
        then
      });
    }
  };
};
function Root(props: any) {
  const { getBaseData, base: { ready = 0 } = {} } = props;
  const [searchParams] = useSearchParams();

  useLayoutEffect(() => {
    // 监听系统主题设置主题
    function theme(scheme: any) {
      if (scheme.matches) {
        // 系统切换到了暗色(dark)主题
        document.documentElement.setAttribute('theme', 'dark');
      } else {
        document.documentElement.setAttribute('theme', 'light');
      }
    }
    // 监听系统主题
    const scheme: any = window.matchMedia('(prefers-color-scheme: dark)');
    theme(scheme);
    // 添加主题变动监控事件
    scheme.addListener(theme);
    // 移除主题变动监控事件
    return () => {
      scheme.removeListener(theme);
    };
  }, []);

  // 获取URL语言参数
  const lang = searchParams.get('lang') || '';

  useLayoutEffect(() => {
    // 请求基础数据
    getBaseData(lang);
  }, [getBaseData, lang]);

  return ready ? (
    <Routes>
      <Route path="/*" element={<Home />}></Route>
      <Route path="/login" element={<Login />}></Route>
      <Route path="/register" element={<Register />}></Route>
    </Routes>
  ) : (
    <Load />
  );
}

export default connect(store, event)(Root);
