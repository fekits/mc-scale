import React, { useState } from 'react';

function Langs(props: any) {
  const { view = 3 } = props;
  const [hide, sethide] = useState(1);
  return hide ? (
    <div
      am-view="aa"
      {...{ view }}
      className="load full"
      mcui-pos="cm"
      onAnimationEnd={() => {
        sethide(0);
      }}>
      <div className="ac mb-mm">
        <p>
          <i className="icon icon-logo"></i>
        </p>
        <p className="nl-sl load-dot">
          loading<span>.</span>
          <span>.</span>
          <span>.</span>
        </p>
      </div>
    </div>
  ) : null;
}

export default Langs;
