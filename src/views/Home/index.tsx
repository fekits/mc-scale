import React, { useLayoutEffect, useState } from 'react';
import { connect } from 'react-redux';

import { Routes, Route, Link, useLocation, useNavigate } from 'react-router-dom';
import { LayerView } from '@fekit/react-layer';
import { HOME_GET_DATA } from '../../actions/home';

import Sidebar from './components/Sidebar';

// import Home from './pages/Home';
import News from './pages/News';
import Order from './pages/Order';
import Finance from './pages/Finance';
import Member from './pages/Member';
import Setting from './pages/Setting';

const store = ({ base }: any) => ({ base });
const event = (dispatch: any) => {
  return {
    getHomeData() {
      dispatch({
        type: HOME_GET_DATA.name
      });
    }
  };
};
function Home(props: any) {
  const { getHomeData, base: { login: { account = '', avatar = {} } = {} } = {} } = props;
  const nav = useNavigate();
  const { search }: any = useLocation();

  const [side, fSide] = useState('open');

  useLayoutEffect(() => {
    if (account) {
    } else {
      nav('/login' + search);
    }
  }, [account, nav, search]);

  useLayoutEffect(() => {
    // getHomeData();
    return () => {};
  }, [getHomeData]);

  // useLayoutEffect(() => {
  //   console.log(base);
  //   return () => {};
  // }, [base]);

  return (
    <div className="user" {...{ side }}>
      <div className="box">
        <Sidebar />
        <div className="user-main">
          <div className="header" mcui-row="">
            <div mcui-col="mob-4" className="header-l al">
              <button
                mcui-btn="@a none l :square"
                onClick={() => {
                  fSide((state) => (state === 'open' ? 'hide' : 'open'));
                }}>
                <i className="icon icon-side"></i>
              </button>
              <button mcui-btn="@a none l :square">
                <i className="icon icon-search"></i>
              </button>
            </div>
            <div mcui-col="mob-20" className="header-r">
              <button mcui-btn="@a none l :flex">
                <img height="12" src="//cms.fekit.cn/assets/img/lang/zh_cn.png" alt="" />
                <span className="ml-ss">简体中文</span>
                <i className="icon icon-dropdown"></i>
              </button>
              <Link to={{ pathname: '/', search }} mcui-btn="@a none l :square">
                <i className="icon icon-home"></i>
              </Link>
              <button mcui-btn="@a none l :square">
                <i className="icon icon-message"></i>
                <sup>0</sup>
              </button>
              <div mcui-btn="@a none l :square :flex">
                <div mcui-img="@a sz:ss :round">
                  <img src={avatar.url} alt={avatar.title} />
                </div>
              </div>
            </div>
          </div>
          <div className="user-middel" mcui-scrollbar=":y">
            <Routes>
              <Route path="/" element={<News />}></Route>
              <Route path="/order" element={<Order />}></Route>
              <Route path="/finance" element={<Finance />}></Route>
              <Route path="/member" element={<Member />}></Route>
              <Route path="/news" element={<News />}></Route>
              <Route path="/setting" element={<Setting />}></Route>
              <Route path="/" element={<News />}></Route>
              <Route path="/" element={<News />}></Route>
              <Route path="/" element={<News />}></Route>
              {/* <Route path="/login" element={<Login />}></Route>
              <Route path="/register" element={<Register />}></Route> */}
            </Routes>
          </div>
        </div>
      </div>
      <LayerView id="user" />
    </div>
  );
}

export default connect(store, event)(Home);
