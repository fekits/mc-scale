import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Link, useLocation } from 'react-router-dom';
import { Swiper, SwiperSlide } from 'swiper/react';
// import { HOME_GET_BASE } from '../../actions/base';
// import Load from '../Load';
import Langs from '../../Login/components/Langs';
import Themes from '../../Login/components/Themes';

const store = ({ base }: any) => ({ base });
const event = (dispatch: any) => {
  return {
    // getBaseData(lang: any, then: any = () => {}) {
    //   dispatch({
    //     type: HOME_GET_BASE.name,
    //     body: { lang },
    //     then
    //   });
    // }
  };
};
function Demo(props: any) {
  const { base: { info = {}, back = {} } = {} } = props;
  // const { search }: any = useLocation();
  const [max, setMax] = useState(false);

  return (
    <div mcui-card={`@a bg:fore sz:m${max ? ' max' : ''}`}>
      <div mcui-card-head="">
        <div>
          <h3 mcui-omit="1">客户管理</h3>
        </div>
        <div mcui-btns="@a s none :square" className="ml-sm-sub">
          <button>
            <i className="icon icon-refresh"></i>
          </button>
          <button>
            <i className="icon icon-filter"></i>
          </button>
          <button
            onClick={() => {
              // maxOrMin();
              setMax((state) => !state);
            }}>
            <i className={`icon ${max ? 'icon-min' : 'icon-max'}`}></i>
          </button>
          <button>
            <i className="co-text icon icon-more"></i>
          </button>
        </div>
      </div>
      <div mcui-card-body="">
        <div className="mb-mm">
          <button mcui-btn="@a main">
            <span>添加联系人</span>
          </button>
        </div>
        <table className="orders-table" mcui-table="@a line l" mcui-form="@a">
          <thead>
            <tr>
              <th>
                <label mcui-checkbox="">
                  <input type="checkbox" />
                  <span></span>
                </label>
              </th>
              <th mcui-hide="=dpc">信息</th>
              <th mcui-hide="<dpc">国家</th>
              <th mcui-hide="<dpc">税号</th>
              <th mcui-hide="<dpc">公司/联系人</th>
              <th mcui-hide="<dpc">
                <p>电话</p>
              </th>
              <th mcui-hide="<dpc">
                <p>地址</p>
              </th>
              <th mcui-hide="<dpc">
                <p className="ar">操作</p>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <label mcui-checkbox="">
                  <input type="checkbox" />
                  <span></span>
                </label>
              </td>
              <td mcui-hide="=dpc">
                <div className="my-sm-sub">
                  <p>状态:</p>
                  <p>
                    <img src="//img.alicdn.com/imgextra/i1/1737167120/TB2Llv1XDIlyKJjSZFrXXXn2VXa_!!1737167120.jpg" />
                  </p>
                  <h5 mcui-omit="2">[TBTM]特价简约电视柜卧室电视柜小户型简易电视柜小型电视柜</h5>
                  <div className="fs-ss">
                    <p>编号： 11621065937178</p>
                    <p>订购日期和时间：2021-05-15 16:05</p>
                    <p>
                      数量: <b>1</b>
                    </p>
                  </div>
                  <div className="mr-sm-sub">
                    <button mcui-btn="@a read :square :border">
                      <i className="icon icon-view"></i>
                    </button>
                    <button mcui-btn="@a risk :square :border">
                      <i className="icon icon-delete"></i>
                    </button>
                  </div>
                </div>
              </td>
              <td mcui-hide="<dpc">状态</td>
              <td mcui-hide="<dpc">
                <p>111</p>
              </td>
              <td mcui-hide="<dpc">
                <h5>李四</h5>
                <p className="fs-ss">某某市某某有限公司</p>
              </td>
              <td mcui-hide="<dpc">
                <p>1</p>
              </td>
              <td mcui-hide="<dpc">
                <p mcui-omit="2">方洲路 中海国际社区1幢</p>
              </td>
              <td mcui-hide="<dpc">
                <div className="ar ml-sm-sub">
                  <button mcui-btn="@a s none :square">
                    <i className="icon co-info icon-view"></i>
                  </button>
                  <button mcui-btn="@a s none :square">
                    <i className="icon co-risk icon-delete"></i>
                  </button>
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <label mcui-checkbox="">
                  <input type="checkbox" />
                  <span></span>
                </label>
              </td>
              <td mcui-hide="=dpc">
                <div className="my-sm-sub">
                  <p>状态:</p>
                  <p>
                    <img
                      src="//img.alicdn.com/imgextra/i1/1737167120/TB2Llv1XDIlyKJjSZFrXXXn2VXa_!!1737167120.jpg"
                      className="el-image__inner"
                      style={{ objectFit: 'fill' }}
                      alt=""
                    />
                  </p>
                  <h5 mcui-omit="2">[TBTM]特价简约电视柜卧室电视柜小户型简易电视柜小型电视柜</h5>
                  <div className="fs-ss">
                    <p>编号： 11621065937178</p>
                    <p>订购日期和时间：2021-05-15 16:05</p>
                    <p>
                      数量: <b>1</b>
                    </p>
                  </div>
                  <div className="mr-sm-sub">
                    <button mcui-btn="@a read :square :border">
                      <i className="icon icon-view"></i>
                    </button>
                    <button mcui-btn="@a risk :square :border">
                      <i className="icon icon-delete"></i>
                    </button>
                  </div>
                </div>
              </td>
              <td mcui-hide="<dpc">状态</td>
              <td mcui-hide="<dpc">
                <p>111</p>
              </td>
              <td mcui-hide="<dpc">
                <h5>张三</h5>
                <p className="fs-ss">某某市某某商贸有限公司</p>
              </td>
              <td mcui-hide="<dpc">
                <p>1</p>
              </td>
              <td mcui-hide="<dpc">
                <p mcui-omit="2">方洲路 中海国际社区1幢</p>
              </td>
              <td mcui-hide="<dpc">
                <div className="ar ml-sm-sub">
                  <button mcui-btn="@a s none :square">
                    <i className="icon icon-view co-info"></i>
                  </button>
                  <button mcui-btn="@a s none :square">
                    <i className="icon icon-delete co-risk"></i>
                  </button>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
        {/* <Pading /> */}
      </div>
      <div mcui-card-foot=""></div>
    </div>
  );
}

export default connect(store, event)(Demo);
