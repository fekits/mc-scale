import React, { useState } from 'react';
import { Link, useLocation, useSearchParams } from 'react-router-dom';

function Sidebar(props: any) {
  const { search }: any = useLocation();

  const menus: any = [
    {
      name: '主工作台',
      icon: 'icon-home',
      path: '/'
    },
    {
      name: '订单管理',
      icon: 'icon-order',
      path: '/order',
      list: [
        {
          name: '物流订单',
          icon: '',
          path: '/order/2'
        },
        {
          name: '代购订单',
          icon: '',
          path: '/order/3'
        }
      ]
    },
    {
      name: '财务管理',
      icon: 'icon-finance',
      path: '/finance',
      list: [
        {
          name: '海外汇款',
          icon: '',
          path: '/finance/1'
        },
        {
          name: 'paypal充值',
          icon: '',
          path: '/finance/2'
        },
        {
          name: '充值记录',
          icon: '',
          path: '/finance/3'
        },
        {
          name: '交易记录',
          icon: '',
          path: '/finance/4'
        }
      ]
    },
    {
      name: '会员管理',
      icon: 'icon-address',
      path: '/member'
    },
    {
      name: '公告管理',
      icon: 'icon-page',
      path: '/news'
    },
    {
      name: '系统设置',
      icon: 'icon-setting',
      path: '/setting',
      list: [
        {
          name: '用户',
          icon: 'icon-address',
          path: '/user'
        },
        {
          name: '外观',
          icon: 'icon-deliver',
          path: '/adorn'
        }
      ]
    }
  ];
  const [menuIndex, fMenu] = useState(0);
  const [twoMenuOpen, fTwoMenu] = useState<boolean>(true);

  return (
    <div className="side">
      <div className="side-mob-hide"></div>
      <div className="side-header vm-sub">
        <i className="fs-lm ml-sm icon icon-logo"></i> <i className="fs-lm side-header-title icon icon-title"></i>
      </div>
      <div className="side-middle">
        <ul className="side-list" mcui-scrollbar=":y">
          {menus.map(({ list = [], path = '', icon = '', name = '' }: any, idx: number) => {
            return (
              <li key={idx} className={idx === menuIndex ? 'active' : ''} data-open={menuIndex === idx && twoMenuOpen ? 1 : 0}>
                <div>
                  <Link
                    to={{ pathname: path, search }}
                    onClick={() => {
                      fMenu(idx);
                    }}>
                    <button mcui-btn="@a none l :square">
                      <i className={`icon ${icon}`}></i>
                    </button>
                    <span className="side-list-item-name">{name}</span>
                    {list.length ? (
                      <i
                        className="side-list-item-sown icon icon-dropdown"
                        onClick={() => {
                          fTwoMenu((state) => !state);
                        }}></i>
                    ) : null}
                  </Link>
                  {list.length ? (
                    <ul className="side-list-tow">
                      {list.map(({ name = '', path = '' }: any, idx: number) => {
                        return (
                          <li key={idx}>
                            <Link to={{ pathname: path, search }}>{name}</Link>
                          </li>
                        );
                      })}
                    </ul>
                  ) : null}
                </div>
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
}

export default Sidebar;
