import React from 'react';
import { connect } from 'react-redux';
// import { DEMO_GET_CONF } from '../../../actions/demo';
import { useSearchParams } from 'react-router-dom';

const store = ({ base }: any) => ({ base });
const event = (dispatch: any) => {
  return {
    // getConfData() {
    //   dispatch({
    //     type: DEMO_GET_CONF.name
    //   });
    // }
  };
};
function Langs(props: any) {
  const { base: { more = {}, lang = {} } = {} } = props;
  const [searchParams, setSearchParams] = useSearchParams();
  const setLang = (lang: any) => {
    console.log(searchParams);
    setSearchParams({ lang });
  };

  const langs = more.lang || {};

  return (
    <ul className="login-langs" mcui-scrollbar=":y">
      {Object.values(langs).map((item: any) => {
        return (
          <li
            key={item.lang}
            className={`ux-click${item.lang === lang.lang ? ' active' : ''}`}
            onClick={() => {
              setLang(item.lang);
            }}>
            <p>
              <img className="lang-img" src={item.icon} alt="" />
              <span>{item.name}</span>
            </p>
          </li>
        );
      })}
    </ul>
  );
}

export default connect(store, event)(Langs);
