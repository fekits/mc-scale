import React from 'react';
import { connect } from 'react-redux';

const store = ({ demo }: any) => ({ demo });
const event = (dispatch: any) => {
  return {};
};
function Demo(props: any) {
  const setTHeme = (theme: any) => {
    console.log('设置主题', theme);
    document.documentElement.setAttribute('theme', theme);
  };

  return (
    <div className="login-theme">
      <p mcui-note="@b warn" className="fs-ss">
        如果您不喜欢当前的主题色，系统为您提供了更多的可选主题色，点击下面按钮切换主题试试？
      </p>
      <ul mcui-row="space mob-6">
        <li>
          <div
            className="login-theme-block ux-click ew2-sub"
            onClick={() => {
              setTHeme('light');
            }}>
            <div className="light-bg-main"></div>
            <div className="light-bg-back"></div>
          </div>
        </li>
        <li>
          <div
            className="login-theme-block ux-click ew2-sub"
            onClick={() => {
              setTHeme('sea');
            }}>
            <div className="sea-bg-main"></div>
            <div className="sea-bg-back"></div>
          </div>
        </li>
        <li>
          <div
            className="login-theme-block ux-click ew2-sub"
            onClick={() => {
              setTHeme('dark');
            }}>
            <div className="dark-bg-main"></div>
            <div className="dark-bg-back"></div>
          </div>
        </li>
      </ul>
    </div>
  );
}

export default connect(store, event)(Demo);
