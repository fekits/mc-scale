import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Link, useLocation, useNavigate, useSearchParams } from 'react-router-dom';
import { Swiper, SwiperSlide } from 'swiper/react';
import { HOME_SSO_LOGIN } from '../../actions/base';
// import Load from '../Load';
import Langs from './components/Langs';
import Themes from './components/Themes';

const store = ({ base }: any) => ({ base });
const event = (dispatch: any) => {
  return {
    ssoLoginData(body: any) {
      dispatch({
        type: HOME_SSO_LOGIN.name,
        body
      });
    }
  };
};
function Demo(props: any) {
  const { ssoLoginData, base: { login = null, info = {}, back = {} } = {} } = props;
  const nav = useNavigate();
  const { search }: any = useLocation();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  useEffect(() => {
    if (login) {
      nav('/' + search);
    }
  }, [login, nav, search]);

  const [searchParams] = useSearchParams();
  // 获取URL语言参数
  const lang = searchParams.get('lang') || '';
  const [swiperRef, setSwiperRef] = useState<any>(null);

  return (
    <div className="login">
      <div className="login-back"></div>
      <div className="login-main" mcui-pos="cm">
        <div className="login-wrap">
          <div className="login-l-decoration"></div>
          <div className="full" mcui-row="" mcui-form="@a">
            <div mcui-col="mob-24 pad-10" className="login-l fw-auto">
              <div>
                <i className="login-logo icon icon-logo"></i>
              </div>
              <div mcui-hide="=mob">
                <h4 className="co-back">{info.name}</h4>
                <p className="co-back fs-ss">{info.version}</p>
                <div className="mt-mm fs-ss">
                  <p>{back.app_copyright_statement}</p>
                </div>
              </div>
            </div>
            <div mcui-col="mob-24 pad-14" className="login-r">
              <div>
                <Swiper
                  className="swiper-no-swiping"
                  onSwiper={setSwiperRef}
                  slidesPerView={1}
                  pagination={{
                    type: 'fraction'
                  }}
                  navigation={true}>
                  <SwiperSlide className="login-slide" mcui-form="@b">
                    <h3>{back.login}</h3>
                    <div>
                      <p className="fs-ss co-note">{back.account}</p>
                      <div {...{ form: 'text :full' }}>
                        <i className="icon icon-username"></i>
                        <input
                          type="text"
                          onChange={(e) => {
                            setUsername(e.target.value);
                          }}
                          placeholder=""
                          defaultValue={username}
                        />
                      </div>
                    </div>
                    <div>
                      <p className="fs-ss co-note">{back.password}</p>
                      <div {...{ form: 'password :full' }}>
                        <i className="icon icon-password"></i>
                        <input
                          type="password"
                          onChange={(e) => {
                            setPassword(e.target.value);
                          }}
                          placeholder=""
                          defaultValue={password}
                        />
                      </div>
                    </div>
                    <div mcui-row="mob-12">
                      <div>
                        <label {...{ form: 'check' }}>
                          <input type="checkbox" />
                          <span>{back.auto_login}</span>
                        </label>
                      </div>
                      <div className="ar co-note">
                        <span className="hover-underline">{back.forgot_password}</span>
                      </div>
                    </div>
                    <div>
                      <button
                        className="mob-full"
                        mcui-btn="@a main mob-l"
                        onClick={() => {
                          ssoLoginData({ lang, username, password });
                        }}>
                        {back.login_now}
                      </button>
                    </div>
                    <div className="co-note">
                      {back.no_account_yet}
                      <Link to={{ pathname: '/register', search }} className="co-link hover-underline">
                        {back.register_now}
                      </Link>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide className="login-slide">
                    <h3 className="login-title">
                      <i
                        className="icon icon-back"
                        onClick={() => {
                          swiperRef.slideTo(0);
                        }}></i>
                      {back.language}
                    </h3>
                    <Langs />
                  </SwiperSlide>
                  <SwiperSlide className="login-slide">
                    <h3 className="login-title">
                      <i
                        className="icon icon-back"
                        onClick={() => {
                          swiperRef.slideTo(0);
                        }}></i>
                      {back.theme}
                    </h3>
                    <Themes />
                  </SwiperSlide>
                </Swiper>
              </div>
              <div className="login-btns mt-sl mr-sl-sub mob-ac" mcui-btns="@a s read :border :square">
                <Link to={{ pathname: '/', search }} className="login-home">
                  <i className="icon icon-home"></i>
                </Link>
                <button
                  className="login-lang"
                  onClick={() => {
                    swiperRef.slideTo(1);
                  }}>
                  <i className="icon icon-lang"></i>
                </button>
                <button
                  className="login-theme"
                  onClick={() => {
                    swiperRef.slideTo(2);
                  }}>
                  <i className="icon icon-theme"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default connect(store, event)(Demo);
