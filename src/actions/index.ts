import * as home from './home';
import * as base from './base';
import * as demo from './demo';

const actions: any = {
  base,
  home,
  demo
};

export default actions;
