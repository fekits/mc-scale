# @FEKIT/SCALE

scale 内核的自适应解决方案，这个方案在一些特殊场景下使用。在大多数场景下建议使用 [https://www.npmjs.com/package/@fekit/mc-ratio](https://www.npmjs.com/package/@fekit/mc-ratio)

## 索引

- [演示](#演示)
- [安装](#安装)
- [参数](#参数)
- [示例](#示例)
- [版本](#版本)

## 演示

- 电脑：[https://fekit.cn/plugins/mc-scale/](https://fekit.cn/devices/#page=home&phone=2&browser=0&group=0&guide=0&url=https%3A%2F%2Ffekit.cn%2Fplugins%2Fmc-scale%2F)
- 手机：[https://fekit.cn/plugins/mc-scale/](https://fekit.cn/plugins/mc-scale/)

## 安装

```
npm i @fekit/mc-scale
```

## 参数

```
    {
        el   : 'id',               {DomObject}   要缩放的DOM对象
        size : [750, 1334],        {Number}      设计稿的尺寸
        full : true | false,       {Boolean}     是否开启全屏模式？如果开启开屏模式则对宽度和高度双向自适应 *
        fixed: 2                   {Number}      设置字号精度为小数点后2
        max  : 750                 {Number}      最大尺寸，超过这个尺寸不再放大
        min  : 320                 {Number}      最小尺寸，超过这个尺寸不再缩小
        then : function () {}      {Function}    自适应字号改变时的回调
    }
```

## 示例

基础自适应：

```javascript
scale({
  el: document.getElementId('area'),
  size: [750, 1334] // 设计稿的尺寸
});
```

双向自适应：

```javascript
scale({
  el: document.getElementId('area'),
  size: [750, 1334], // 设计稿的尺寸
  full: true, // 开启全屏模式，将对宽度和高度双向自适应缩放，这种模式一般用于无滚动条的全屏页面。
  fixed: 2 // 设置字号精度为小数点后2位
});
```

限制最大尺寸：

```javascript
scale({
  el: document.getElementId('area'),
  size: [750, 1334], // 设计稿的尺寸
  full: true, // 开启全屏模式，将对宽度和高度双向自适应缩放，这种模式一般用于无滚动条的全屏页面。
  max: 750 // 超过750的尺寸不再放大
});
```

局部自适应

```javascript
// 以下示例仅对指定ID为area的元素做自适应，需要注意的是，如果你仅对网页中的部分区域做自适应，请勿开启dpr功能，该功能将影响全局网页
scale({
  el: document.getElementId('area'), // 为ID为area的标签设置自适应字号，当字号不是设置在HTML根标签时，只能在区域内用em方案自适应
  size: [750, 1334], // 设计稿的尺寸
  fixed: 2
});
```

## 版本
```
v2.0.0 [Latest version]
TS 重构
```

```
v1.0.5 [Latest version]
1、非功能性升级
```

```
v1.0.4
1、非功能性升级
```

```
v1.0.3
1、添加min和max属性,大于或小于某个值后不再缩放
```

```
v1.0.0
1、由@fekit/mc-ratio修改而成。继承了@fekit/mc-ratio的所有入参及功能
```
