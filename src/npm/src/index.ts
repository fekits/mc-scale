/**
 * 版本：v1.1.0
 * 文档：请参阅 https://gitlab.com/fekits/mc-scale/blob/master/readme.md
 * */

const scale = function (o: any = {}) {
  let doc: any = document.documentElement;
  // console.log(doc);
  // 设置字号的标签，如果没有传参则默认为根标签
  let el = o.el || doc;
  // console.log(el);

  // 设计稿的宽度
  let design_w = o.size[0];

  // 设计稿的高度
  let design_h = o.size[1];

  // 设计稿的比例
  let design_r = design_h / design_w;

  // 最大缩放比例
  let max = o.max;
  // 最小缩放比例
  let min = o.min;

  // 是否双向检测
  let full = o.full;

  let _ratio = function () {
    let DPR = window.devicePixelRatio;
    // 设置DPR
    if (o.dpr) {
      document
        .getElementsByName('viewport')[0]
        ?.setAttribute('content', `width=device-width,initial-scale=1,minimum-scale=${1 / DPR},maximum-scale=${1 / DPR},user-scalable=no`);
    }

    // 浏览器的宽度
    let window_w = doc.clientWidth;

    // 浏览器的高度
    let window_h = doc.clientHeight;

    // 最大尺寸和最小尺寸
    if (max && window_w > max) {
      window_w = max;
    }
    if (min && window_w < min) {
      window_h = window_h * (min / window_w);
      window_w = min;
    }

    // 字号;
    let fontSize;

    if (full) {
      /*
        如果浏览器的高宽比例小于设计稿的高度比例时，那如果还仅按照宽度缩放的话，内容肯定是放不下的。
        这就好比设计尺寸是100*200的比例，内容是一个80*180的色块，当浏览器尺寸是50*50时。
        就算是内容宽度跟据宽度比例缩小了一半是40，那内容高度按比例缩小到90也不能在浏览器50*50的尺寸内放展示出来。
      */
      if (window_h / window_w < design_r) {
        // 字号 = 浏览器的高度 ／ 设计稿的比例 ／ 设计稿的宽度
        fontSize = window_h / design_r / design_w;
      } else {
        fontSize = window_w / design_w;
      }
    } else {
      // 字号 = （浏览器的宽度 ／ 设计稿的宽度）*100
      fontSize = window_w / design_w;
    }
    fontSize = fontSize > o.maxScale ? o.maxScale : fontSize;
    fontSize = fontSize < o.minScale ? o.minScale : fontSize;

    fontSize = fontSize.toFixed(o.fixed || 6);
    fontSize = parseFloat(fontSize);
    el.style.transform = 'scale(' + fontSize + ')';
    o.then && o.then(fontSize);
  };

  _ratio();
  window.addEventListener('resize', _ratio);
};
export default scale;
